import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    let { shoe, hanhdleViewDetail,handleAddToCart } = this.props;
    let { name, image, price } = shoe;
    return (
      <div className="col-3 my-3">
        <div className="card text-left">
          <img className="card-img-top" src={image} alt="picture item" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{price}</p>
            <button
              className="btn btn-success mr-5"
              onClick={() => {
                hanhdleViewDetail(shoe);
              }}
            >
              Detail Shoe
            </button>
            <button className="btn btn-primary" onClick={() => { handleAddToCart(shoe) }}>Add To Cart</button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDisPatchToProps = (dispatch) => {
  return {
    hanhdleViewDetail: (item) => {
      let action = {
        type: "VIEW_DETAIL",
        payload: item,
      };
      dispatch(action);
    },
    handleAddToCart: (item) => { 
      let action = {
        type: "ADD_CART",
        payload: item,
      }
      dispatch(action);
     },
  };
};

export default connect(null, mapDisPatchToProps)(ItemShoe);
