import React, { Component } from "react";
import { connect } from "react-redux";

class CartShoe extends Component {
  render() {
    let { cart,handleDeleteItem,handleChangAmount } = this.props;
    return (
      <div>
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Amount</th>
              <th>Price</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((shoe) => {
              let { name, amount, price, image, id } = shoe;
              return (
                <tr key={id}>
                  <td scope="row">{name}</td>
                  <td>
                    <button className="btn btn-warning mx-2" onClick={() => { handleChangAmount(shoe, -1) }}>-</button>
                    {amount}
                    <button className="btn btn-info mx-2" onClick={() => { handleChangAmount(shoe, 1) }}>+</button>

                    </td>
                  <td>{price *  amount}</td>
                  <td><img src= {image} alt="picture cart" style={{width:50}} /></td>
                  <button className="btn btn-danger" onClick={() => { handleDeleteItem(shoe) }}>X</button>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};


let mapDispatchToProps = (dispatch) => { 
  return {
    handleDeleteItem: (item) => { 
      let action = {
      type: "DELETE_ITEM",
      payload: item,
      }
      dispatch(action);
     },
     handleChangAmount: (item, option) => { 
      let aciton = {
        type: "CHANGE_AMONT",
        payload: {item,option}
      }
      dispatch(aciton)
      }
  }
 }

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
