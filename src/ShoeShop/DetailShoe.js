import React, { Component } from 'react'
import { connect } from 'react-redux'

 class DetailShoe extends Component {
  render() {
    let {detail} = this.props;
    let {name, price, description, quantity} = detail
    return (
      <div>
      <table class="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Stock</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td scope="row">{name}</td>
            <td>{price}</td>
            <td>{description}</td>
            <td>{quantity}</td>
          </tr> 
        </tbody>
      </table>
      </div>
    )
  }
}


let mapStateToProps = ( state) => { 
  return {
    detail: state.shoeReducer.detail,
  }
 }

 export default connect (mapStateToProps, null) (DetailShoe)
 