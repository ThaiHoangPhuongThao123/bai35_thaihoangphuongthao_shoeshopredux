import React, { Component } from 'react'
import CartShoe from './CartShoe'
import ListShoe from './ListShoe'
import DetailShoe from './DetailShoe'

export default class ShoeShop extends Component {
  render() {
    return (
      <div>
        <CartShoe/>
        <ListShoe/>
        <DetailShoe/>
      </div>
    )
  }
}
