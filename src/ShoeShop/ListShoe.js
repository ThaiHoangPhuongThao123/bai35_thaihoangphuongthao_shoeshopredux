import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './ItemShoe';
 class ListShoe extends Component {
  renderListShoe = () => { 
    let {list} = this.props;
    return list.map((item) => { 
      return <ItemShoe shoe = {item} key={item.id}/>
     })
   }
  render() {
    return (
      <div className='row'>
        {this.renderListShoe()}
      </div>
    )
  }
}


let mapStateToProps = (state) => { 
  return {
    list: state.shoeReducer.shoeArr,
  }
 }

 export default connect(mapStateToProps, null) (ListShoe)