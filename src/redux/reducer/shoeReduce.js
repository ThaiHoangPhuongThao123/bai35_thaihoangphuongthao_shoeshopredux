import { shoeArr } from "../../ShoeShop/data";
import { ADD_CART, CHANGE_AMONT, DELETE_ITEM, VIEW_DETAIL } from "../constant/ShoeShopConstant";

let initialState = {
  shoeArr: shoeArr,
  detail: shoeArr[0],
  cart: [],
};
export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_DETAIL: {
      state.detail = payload;
      return { ...state };
    }
    case ADD_CART: {
      // th1: nếu sản phẩm khoong có trong giỏ hàng 
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => { 
        return item.id === payload.id;
       })
       if (index === -1) {
        let newShhoe = {...payload, amount: 1};
        cloneCart.push(newShhoe);
       } else {
        cloneCart[index].amount += 1;
       }
       state.cart = cloneCart;
       console.log('cloneCart: ', cloneCart);
       return {...state}

    }
    case DELETE_ITEM: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => { 
        return item.id === payload.id
       })
       if (index !== -1) {
        cloneCart.splice(index, 1)
       }
       state.cart = cloneCart;
       return {...state}
    }
    case CHANGE_AMONT: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => { 
        return item.id === payload.item.id;
       })
       if (index !== -1) {
        cloneCart[index].amount += payload.option;
        if (cloneCart[index].amount < 1) {
          cloneCart.splice(index, 1) 
        }
        state.cart = cloneCart;
        return {...state}
       }
    }
    default:
      return state;
  }
};
