import { combineReducers } from 'redux';
import { shoeReducer } from './shoeReduce';
export const rootReducer = combineReducers({
    shoeReducer: shoeReducer,
})